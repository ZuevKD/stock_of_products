package ru.zuevkirill.stock.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import ru.zuevkirill.stock.entities.Product;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.List;

public class ProductDao implements CrudDao<Product> {
    private static final String TABLE = "products";
    private static final String ID = "id_product";
    private static final String NAME_PRODUCT = "name_product";
    private static final String AMOUNT = "amount";
    private static final String PRICE = "price";
    private static final String[] PK_COLUMN = {ID};

    private static final String SQL_INSERT = "INSERT INTO " + TABLE
            + " ("
            + NAME_PRODUCT + ","
            + AMOUNT + ","
            + PRICE
            + ") VALUES(?,?,?)";

    private static final String SQL_UPDATE = "UPDATE " + TABLE
            + " SET "
            + NAME_PRODUCT + " = ?,"
            + AMOUNT + " = ?,"
            + PRICE + " = ?"
            + "WHERE " + ID + " = ?";

    private static final String SQL_DELETE = "DELETE FROM " + TABLE + " WHERE " + ID + " = ?";
    private static final String SELECT = "SELECT * FROM " + TABLE + " WHERE " + ID + " = ?";
    private static final String SELECT_ALL = "SELECT * FROM " + TABLE;

    private JdbcTemplate template;

    public ProductDao(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }

    @Override
    public Long add(Product product) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        template.update(connection -> {
            final PreparedStatement ps = connection.prepareStatement(SQL_INSERT, PK_COLUMN);
            ps.setString(1, product.getName());
            ps.setInt(2, product.getAmount());
            ps.setBigDecimal(3, product.getPrice());

            return ps;
        }, keyHolder);

        return keyHolder.getKey().longValue();
    }

    @Override
    public void update(long id, Product product) {
        template.update(SQL_UPDATE,
                product.getName(),
                product.getAmount(),
                product.getPrice(),
                id);
    }

    @Override
    public void remove(long id) {
        template.update(SQL_DELETE, id);
    }

    @Override
    public List<Product> getRecords() {
        return template.query(SELECT_ALL,
                (rs, rowNum) -> new Product(
                        rs.getLong(ID),
                        rs.getString(NAME_PRODUCT),
                        rs.getInt(AMOUNT),
                        rs.getBigDecimal(PRICE)
                ));
    }

    @Override
    public Product getRecord(long id) {
        return template.queryForObject(SELECT, new Object[]{id},
                (rs, rowNum) -> new Product(
                        rs.getLong(ID),
                        rs.getString(NAME_PRODUCT),
                        rs.getInt(AMOUNT),
                        rs.getBigDecimal(PRICE)
                ));
    }
}