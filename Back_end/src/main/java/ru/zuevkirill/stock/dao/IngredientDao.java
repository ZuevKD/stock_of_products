package ru.zuevkirill.stock.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import ru.zuevkirill.stock.entities.Dish;
import ru.zuevkirill.stock.entities.Ingredient;
import ru.zuevkirill.stock.entities.Product;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.List;

public class IngredientDao implements CrudDao<Ingredient> {
    private static final String TABLE = "ingredients";
    private static final String ID = "id_ingredient";
    private static final String AMOUNT = "amount";
    private static final String ID_DISH = "id_dish";
    private static final String ID_PRODUCT = "id_product";
    private static final String NAME_DISH = "name_dish";
    private static final String COSTS = "costs";
    private static final String NAME_PRODUCT = "name_product";
    private static final String PRICE = "price";
    private static final String[] PK_COLUMN = {ID};

    private static final String SQL_INSERT = "INSERT INTO " + TABLE
            + " ("
            + AMOUNT + ","
            + ID_DISH + ","
            + ID_PRODUCT
            + ") VALUES(?,?,?)";

    private static final String SQL_UPDATE = "UPDATE " + TABLE
            + " SET "
            + AMOUNT + " = ?,"
            + ID_DISH + " = ?,"
            + ID_PRODUCT + " = ?"
            + "WHERE " + ID + " = ?";

    private static final String SQL_DELETE = "DELETE FROM " + TABLE + " WHERE " + ID_DISH + " = ?";
    private static final String SELECT = "SELECT id_ingredient, i.id_dish, name_dish, costs, p.id_product, name_product, i.amount, price\n" +
            "FROM dishes\n" +
            "JOIN ingredients i on dishes.id_dish = i.id_dish\n" +
            "JOIN products p on i.id_product = p.id_product\n" +
            "WHERE i.id_dish = ?";
    private static final String SELECT_ALL = "SELECT * FROM " + TABLE;

    private JdbcTemplate template;

    public IngredientDao(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }

    @Override
    public Long add(Ingredient ingredient) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        template.update(connection -> {
            final PreparedStatement ps = connection.prepareStatement(SQL_INSERT, PK_COLUMN);
            ps.setInt(1, ingredient.getAmount());
            ps.setLong(2, ingredient.getDish().getId());
            ps.setLong(3, ingredient.getProduct().getId());

            return ps;
        }, keyHolder);

        return keyHolder.getKey().longValue();
    }

    @Override
    public void update(long id, Ingredient ingredient) {
        template.update(SQL_UPDATE,
                ingredient.getAmount(),
                ingredient.getDish(),
                ingredient.getProduct(),
                id);
    }

    @Override
    public void remove(long dishId) {
        template.update(SQL_DELETE, dishId);
    }

    @Override
    public List<Ingredient> getRecords() {
        return null;
//        return template.query(SELECT_ALL,
//                (rs, rowNum) -> new Ingredient(
//                        rs.getLong(ID),
//                        rs.getLong(ID_DISH),
//                        rs.getLong(ID_PRODUCT),
//                        rs.getInt(AMOUNT)
//                ));
    }

    public List<Ingredient> getRecordsWithIngredients(long dishId) {
        return template.query(SELECT, new Object[]{dishId},
                (rs, rowNum) -> {
                    Dish dish = new Dish(
                            rs.getLong(ID_DISH),
                            rs.getString(NAME_DISH),
                            rs.getBigDecimal(COSTS)
                    );

                    Product product = new Product(
                            rs.getLong(ID_PRODUCT),
                            rs.getString(NAME_PRODUCT),
                            rs.getInt(AMOUNT),
                            rs.getBigDecimal(PRICE)
                    );

                    return new Ingredient(
                            rs.getLong(ID),
                            dish,
                            product,
                            rs.getInt(AMOUNT)
                    );
                });
    }

    @Override
    public Ingredient getRecord(long id) {
        return null;
//        return template.queryForObject(SELECT, new Object[]{id},
//                (rs, rowNum) -> new Ingredient(
//                        rs.getLong(ID),
//                        rs.getLong(ID_DISH),
//                        rs.getLong(ID_PRODUCT),
//                        rs.getInt(AMOUNT)
//                ));
    }
}
