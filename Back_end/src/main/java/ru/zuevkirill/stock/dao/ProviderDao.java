package ru.zuevkirill.stock.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import ru.zuevkirill.stock.entities.Provider;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.List;

public class ProviderDao implements CrudDao<Provider> {
    private static final String TABLE = "providers";
    private static final String ID = "id_provider";
    private static final String NAME_PROVIDER = "name_provider";
    private static final String[] PK_COLUMN = {ID};

    private static final String SQL_INSERT = "INSERT INTO " + TABLE
            + " ("
            + NAME_PROVIDER
            + ") VALUES(?)";

    private static final String SQL_UPDATE = "UPDATE " + TABLE
            + " SET "
            + NAME_PROVIDER + " = ?"
            + "WHERE " + ID + " = ?";

    private static final String SQL_DELETE = "DELETE FROM " + TABLE + " WHERE " + ID + " = ?";
    private static final String SELECT = "SELECT * FROM " + TABLE + " WHERE " + ID + " = ?";
    private static final String SELECT_ALL = "SELECT * FROM " + TABLE;

    private JdbcTemplate template;

    public ProviderDao(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }

    @Override
    public Long add(Provider provider) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        template.update(connection -> {
            final PreparedStatement ps = connection.prepareStatement(SQL_INSERT, PK_COLUMN);
            ps.setString(1, provider.getName());
            return ps;
        }, keyHolder);

        return keyHolder.getKey().longValue();
    }

    @Override
    public void update(long id, Provider provider) {
        template.update(SQL_UPDATE, provider.getName(), id);
    }

    @Override
    public void remove(long id) {
        template.update(SQL_DELETE, id);
    }

    @Override
    public List<Provider> getRecords() {
        return template.query(SELECT_ALL,
                (rs, rowNum) -> new Provider(
                        rs.getLong(ID),
                        rs.getString(NAME_PROVIDER)
                ));
    }

    @Override
    public Provider getRecord(long id) {
        return template.queryForObject(SELECT, new Object[]{id},
                (rs, rowNum) ->new Provider(
                        rs.getLong(ID),
                        rs.getString(NAME_PROVIDER)
                ));
    }
}
