package ru.zuevkirill.stock.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import ru.zuevkirill.stock.entities.Dish;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.List;

public class DishDao implements CrudDao<Dish> {
    private static final String TABLE = "dishes";
    private static final String ID = "id_dish";
    private static final String NAME_DISH = "name_dish";
    private static final String COSTS = "costs";
    private static final String[] PK_COLUMN = {ID};

    private static final String SQL_INSERT = "INSERT INTO " + TABLE
            + " ("
            + NAME_DISH + ","
            + COSTS
            + ") VALUES(?,?)";

    private static final String SQL_UPDATE = "UPDATE " + TABLE
            + " SET "
            + NAME_DISH + " = ?,"
            + COSTS + " = ?"
            + "WHERE " + ID + " = ?";

    private static final String SQL_DELETE = "DELETE FROM " + TABLE + " WHERE " + ID + " = ?";
    private static final String SELECT = "SELECT * FROM " + TABLE + " WHERE " + ID + " = ?";
    private static final String SELECT_ALL = "SELECT * FROM " + TABLE;

    private JdbcTemplate template;

    public DishDao(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }

    @Override
    public Long add(Dish dish) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        template.update(connection -> {
            final PreparedStatement ps = connection.prepareStatement(SQL_INSERT, PK_COLUMN);
            ps.setString(1, dish.getName());
            ps.setBigDecimal(2, dish.getCosts());

            return ps;
        }, keyHolder);

        return keyHolder.getKey().longValue();
    }

    @Override
    public void update(long id, Dish dish) {
        template.update(SQL_UPDATE,
                dish.getName(),
                dish.getCosts(),
                id);
    }

    @Override
    public void remove(long id) {
        template.update(SQL_DELETE, id);
    }

    @Override
    public List<Dish> getRecords() {
        return template.query(SELECT_ALL,
                (rs, rowNum) -> new Dish(
                        rs.getLong(ID),
                        rs.getString(NAME_DISH),
                        rs.getBigDecimal(COSTS)
                ));
    }

    @Override
    public Dish getRecord(long id) {
        return template.queryForObject(SELECT, new Object[]{id},
                (rs, rowNum) -> new Dish(
                        rs.getLong(ID),
                        rs.getString(NAME_DISH),
                        rs.getBigDecimal(COSTS)
                ));
    }
}
