package ru.zuevkirill.stock.dao;

import java.util.List;

public interface CrudDao<T> {
    Long add(T product);

    void update(long id, T product);

    void remove(long id);

    List<T> getRecords();

    T getRecord(long id);
}
