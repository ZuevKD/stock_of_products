package ru.zuevkirill.stock.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import ru.zuevkirill.stock.app.Utils;

import java.math.BigDecimal;

public class CommonRequests {
    private static JdbcTemplate template = new JdbcTemplate(Utils.getDataSource());
    private static String SELECT_SUM = "" +
            "SELECT\n" +
            "       sum(ingredients.amount * p.price) + costs\n" +
            "              + ((sum(ingredients.amount * p.price)\n" +
            "                        + costs)*((SELECT precent FROM overheads) / 100)) as costs\n" +
            "FROM\n" +
            "     ingredients\n" +
            "            join products p on ingredients.id_product = p.id_product\n" +
            "            join dishes d on ingredients.id_dish = d.id_dish\n" +
            "WHERE d.id_dish = ?;";

    public static BigDecimal getSumDish(long dishId) {
        return template.queryForObject(SELECT_SUM, new Object[]{dishId},
                (rs, rowNum) -> rs.getBigDecimal("costs").setScale(2,  BigDecimal.ROUND_FLOOR));
    }
}
