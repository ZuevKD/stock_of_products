package ru.zuevkirill.stock.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import ru.zuevkirill.stock.entities.Acceptance;
import ru.zuevkirill.stock.entities.Dish;
import ru.zuevkirill.stock.entities.Selling;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.List;

public class SellingDao implements CrudDao<Selling> {
    private static final String TABLE = "selling";
    private static final String ID = "id_selling";
    private static final String DATE = "date_selling";
    private static final String ID_DISH = "id_dish";
    private static final String TOTAL_COSTS = "total_costs";
    private static final String AMOUNT = "amount";
    private static final String NAME_DISH = "name_dish";
    private static final String COSTS = "costs";
    private static final String[] PK_COLUMN = {ID};

    private static final String SQL_INSERT = "INSERT INTO " + TABLE
            + " ("
            + AMOUNT + ","
            + ID_DISH + ","
            + DATE + ","
            + TOTAL_COSTS
            + ") VALUES(?,?,?,?)";

    private static final String SQL_UPDATE = "UPDATE " + TABLE
            + " SET "
            + AMOUNT + " = ?,"
            + ID_DISH + " = ?,"
            + DATE + " = ?,"
            + TOTAL_COSTS + " = ?"
            + "WHERE " + ID + " = ?";

    private static final String SQL_DELETE = "DELETE FROM " + TABLE + " WHERE " + ID + " = ?";
    private static final String SELECT = "SELECT * FROM " + TABLE + " WHERE " + ID + " = ?";
    private static final String SELECT_ALL = "" +
            "SELECT id_selling, amount, d.id_dish, name_dish, costs, date_selling, total_costs\n" +
            "FROM selling\n" +
            "       join dishes d on selling.id_dish = d.id_dish\n";

    private JdbcTemplate template;

    public SellingDao(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }

    @Override
    public Long add(Selling selling) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        template.update(connection -> {
            final PreparedStatement ps = connection.prepareStatement(SQL_INSERT, PK_COLUMN);
            ps.setInt(1, selling.getAmount());
            ps.setLong(2, selling.getDish().getId());
            ps.setDate(3, selling.getDate());
            ps.setBigDecimal(4, selling.getAmountCosts());

            return ps;
        }, keyHolder);

        return keyHolder.getKey().longValue();
    }

    @Override
    public void update(long id, Selling selling) {
        template.update(SQL_UPDATE,
                selling.getAmount(),
                selling.getDish().getId(),
                selling.getDate(),
                selling.getAmountCosts(),
                id);
    }

    @Override
    public void remove(long id) {
        template.update(SQL_DELETE, id);
    }

    @Override
    public List<Selling> getRecords() {
        return template.query(SELECT_ALL,
                (rs, rowNum) -> {
                    Dish dish = new Dish(
                            rs.getLong(ID_DISH),
                            rs.getString(NAME_DISH),
                            rs.getBigDecimal(COSTS)
                    );

                    return new Selling(
                            rs.getLong(ID),
                            rs.getInt(AMOUNT),
                            dish,
                            rs.getDate(DATE),
                            rs.getBigDecimal(TOTAL_COSTS)
                    );
                });
    }

    @Override
    public Selling getRecord(long id) {
        return template.queryForObject(SELECT, new Object[]{id},
                (rs, rowNum) -> {
                    Dish dish = new Dish(
                            rs.getLong(ID_DISH),
                            rs.getString(NAME_DISH),
                            rs.getBigDecimal(COSTS)
                    );

                    return new Selling(
                            rs.getLong(ID),
                            rs.getInt(AMOUNT),
                            dish,
                            rs.getDate(DATE),
                            rs.getBigDecimal(TOTAL_COSTS)
                    );
                });
    }
}
