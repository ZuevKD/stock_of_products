package ru.zuevkirill.stock.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import ru.zuevkirill.stock.entities.Acceptance;
import ru.zuevkirill.stock.entities.Product;
import ru.zuevkirill.stock.entities.Provider;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.List;

public class AcceptanceDao implements CrudDao<Acceptance> {
    private static final String TABLE = "acceptance";
    private static final String ID = "id_acceptance";
    private static final String DATE = "date_accept";
    private static final String ID_PROVIDER = "id_provider";
    private static final String ID_PRODUCT = "id_product";
    private static final String AMOUNT = "amount";
    private static final String NAME_PROVIDER = "name_provider";
    private static final String NAME_PRODUCT = "name_product";
    private static final String AMOUNT_ON_STOCK = "amount_on_stock";
    private static final String PRICE = "price";
    private static final String[] PK_COLUMN = {ID};

    private static final String SQL_INSERT = "INSERT INTO " + TABLE
            + " ("
            + DATE + ","
            + ID_PROVIDER + ","
            + ID_PRODUCT + ","
            + AMOUNT
            + ") VALUES(?,?,?,?)";

    private static final String SQL_UPDATE = "UPDATE " + TABLE
            + " SET "
            + DATE + " = ?,"
            + ID_PROVIDER + " = ?,"
            + ID_PRODUCT + " = ?,"
            + AMOUNT + " = ?"
            + "WHERE " + ID + " = ?";

    private static final String SQL_DELETE = "DELETE FROM " + TABLE + " WHERE " + ID + " = ?";
    private static final String SELECT = "SELECT * FROM " + TABLE + " WHERE " + ID + " = ?";
    private static final String SELECT_ALL = "" +
            "SELECT id_acceptance, date_accept, p2.id_provider, name_provider, acceptance.amount, " +
            "p.id_product, name_product, p.amount as amount_on_stock, price " +
            "FROM acceptance" +
            "       JOIN products p on acceptance.id_product = p.id_product" +
            "       JOIN providers p2 on acceptance.id_provider = p2.id_provider";

    private JdbcTemplate template;

    public AcceptanceDao(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }

    @Override
    public Long add(Acceptance acceptance) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        template.update(connection -> {
            final PreparedStatement ps = connection.prepareStatement(SQL_INSERT, PK_COLUMN);
            ps.setDate(1, acceptance.getAcceptDate());
            ps.setLong(2, acceptance.getProvider().getId());
            ps.setLong(3, acceptance.getProduct().getId());
            ps.setInt(4, acceptance.getAmount());

            return ps;
        }, keyHolder);

        return keyHolder.getKey().longValue();
    }

    @Override
    public void update(long id, Acceptance acceptance) {
        template.update(SQL_UPDATE,
                acceptance.getAcceptDate(),
                acceptance.getProvider().getId(),
                acceptance.getProduct().getId(),
                acceptance.getAmount(),
                id);
    }

    @Override
    public void remove(long id) {
        template.update(SQL_DELETE, id);
    }

    @Override
    public List<Acceptance> getRecords() {
        return template.query(SELECT_ALL,
                (rs, rowNum) -> {
                    Provider provider = new Provider(
                            rs.getLong(ID_PROVIDER),
                            rs.getString(NAME_PROVIDER)
                    );

                    Product product = new Product(
                            rs.getLong(ID_PRODUCT),
                            rs.getString(NAME_PRODUCT),
                            rs.getInt(AMOUNT_ON_STOCK),
                            rs.getBigDecimal(PRICE)
                    );

                    return new Acceptance(
                            rs.getLong(ID),
                            rs.getDate(DATE),
                            provider,
                            product,
                            rs.getInt(AMOUNT)
                    );
                });
    }

    @Override
    public Acceptance getRecord(long id) {
        return template.queryForObject(SELECT, new Object[]{id},
                (rs, rowNum) -> {
                    Provider provider = new Provider(
                            rs.getLong(ID_PROVIDER),
                            rs.getString(NAME_PROVIDER)
                    );

                    Product product = new Product(
                            rs.getLong(ID_PRODUCT),
                            rs.getString(NAME_PRODUCT),
                            rs.getInt(AMOUNT_ON_STOCK),
                            rs.getBigDecimal(PRICE)
                    );

                    return new Acceptance(
                            rs.getLong(ID),
                            rs.getDate(DATE),
                            provider,
                            product,
                            rs.getInt(AMOUNT)
                    );
                });
    }
}
