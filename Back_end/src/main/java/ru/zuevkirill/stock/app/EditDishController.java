package ru.zuevkirill.stock.app;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import ru.zuevkirill.stock.dao.DishDao;
import ru.zuevkirill.stock.dao.IngredientDao;
import ru.zuevkirill.stock.dao.ProductDao;
import ru.zuevkirill.stock.entities.Dish;
import ru.zuevkirill.stock.entities.Ingredient;
import ru.zuevkirill.stock.entities.Product;

import java.math.BigDecimal;
import java.util.regex.Pattern;

public class EditDishController {
    @FXML
    private Button button;
    @FXML
    private TextField amount;
    @FXML
    private ChoiceBox<Product> product;
    @FXML
    private Button addIngriedient;
    @FXML
    private TableColumn<Product, String> columnAmount;
    @FXML
    private TextField name;
    @FXML
    private TableView<Ingredient> table;
    @FXML
    private TableColumn<Product, String> columnName;
    @FXML
    private TextField costs;

    private AdminAppController adminAppController;
    private ProductDao productDao;
    private DishDao dishDao;
    private IngredientDao ingredientDao;
    Dish selectedDish;

    private ObservableList<Product> products = FXCollections.observableArrayList();
    private ObservableList<Ingredient> ingredients = FXCollections.observableArrayList();


    @FXML
    void initialize() {
        columnName.setCellValueFactory(new PropertyValueFactory<>("product"));
        columnAmount.setCellValueFactory(new PropertyValueFactory<>("amount"));

        SimpleDriverDataSource ds = Utils.getDataSource();

        productDao = new ProductDao(ds);
        dishDao = new DishDao(ds);
        ingredientDao = new IngredientDao(ds);

        Pattern p = Pattern.compile("(\\d+\\.?\\d*)?");

        amount.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!p.matcher(newValue).matches()) amount.setText(oldValue);
        });

    }

    public void setAdminController(AdminAppController adminController) {
        this.adminAppController = adminController;
    }

    public void setDishId(long dishId) {
        selectedDish = dishDao.getRecord(dishId);

        name.setText(selectedDish.getName());
        costs.setText(String.valueOf(selectedDish.getCosts()));

        products.addAll(productDao.getRecords());
        product.setItems(products);

        addIngriedient.setOnAction(event -> {
            ingredientDao.add(new Ingredient(
                    dishDao.getRecord(dishId),
                    product.getValue(),
                    Integer.valueOf(amount.getText())
            ));

            ingredients.clear();
            ingredients.addAll(ingredientDao.getRecordsWithIngredients(dishId));
            table.setItems(ingredients);
        });
    }

    public void setTypeEditor(TypeEditor typeEditor) {
        if (TypeEditor.ADD == typeEditor) {
            button.setText("Добавить");

            addIngriedient.setDisable(true);

            button.setOnAction(event -> {
                dishDao.add(new Dish(
                        name.getText(),
                        BigDecimal.valueOf(Double.valueOf(costs.getText()))
                ));

                Stage stage = (Stage) button.getScene().getWindow();
                stage.close();

                adminAppController.refreshDishTable();
            });
        } else {
            button.setText("Изменить");

            addIngriedient.setDisable(false);

            button.setOnAction(event -> {
                dishDao.update(selectedDish.getId(), new Dish(
                        name.getText(),
                        BigDecimal.valueOf(Double.valueOf(costs.getText()))
                ));

                Stage stage = (Stage) button.getScene().getWindow();
                stage.close();

                adminAppController.refreshDishTable();
            });
        }
    }
}
