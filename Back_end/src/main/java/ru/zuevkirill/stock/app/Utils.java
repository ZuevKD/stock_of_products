package ru.zuevkirill.stock.app;

import com.mysql.cj.jdbc.Driver;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import java.sql.SQLException;

public class Utils {
    private static final String URL = "jdbc:mysql://localhost:3306/stock_of_product" +
            "?verifyServerCertificate=false" +
            "&useSSL=false" +
            "&requireSSL=false" +
            "&useLegacyDatetimeCode=false" +
            "&allowPublicKeyRetrieval=true" +
            "&amp" +
            "&serverTimezone=UTC";
    private static final String USER = "root";
    private static final String PASSWORD = "root";

    private static SimpleDriverDataSource ds;

    private Utils() {
    }

    public static SimpleDriverDataSource getDataSource() {
        if (ds == null) {
            ds = new SimpleDriverDataSource();
            try {
                ds.setDriver(new Driver());
            } catch (SQLException e) {
                e.printStackTrace();
            }
            ds.setUrl(URL);
            ds.setUsername(USER);
            ds.setPassword(PASSWORD);
        }

        return ds;
    }
}
