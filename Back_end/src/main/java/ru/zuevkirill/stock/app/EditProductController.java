package ru.zuevkirill.stock.app;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import ru.zuevkirill.stock.dao.ProductDao;
import ru.zuevkirill.stock.entities.Product;

import java.math.BigDecimal;
import java.util.regex.Pattern;

public class EditProductController {
    @FXML
    private Button button;
    @FXML
    private TextField amount;
    @FXML
    private TextField price;
    @FXML
    private TextField name;

    private AdminAppController adminAppController;
    private ProductDao productDao;
    Product selectedProduct;

    @FXML
    void initialize() {
        SimpleDriverDataSource ds = Utils.getDataSource();

        productDao = new ProductDao(ds);

        Pattern p = Pattern.compile("(\\d+\\.?\\d*)?");

        amount.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!p.matcher(newValue).matches()) amount.setText(oldValue);
        });

        price.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!p.matcher(newValue).matches()) price.setText(oldValue);
        });
    }

    public void setProductId(long productId) {
        selectedProduct = productDao.getRecord(productId);

         name.setText( selectedProduct.getName());
         amount.setText(Integer.toString(selectedProduct.getAmount()));
         price.setText(String.valueOf(selectedProduct.getPrice()));
    }

    public void setAdminController(AdminAppController adminController) {
        this.adminAppController = adminController;
    }

    public void setTypeEditor(TypeEditor typeEditor) {
        if (TypeEditor.ADD == typeEditor) {
            button.setText("Добавить");

            button.setOnAction(event -> {
                productDao.add(new Product(
                        name.getText(),
                        Integer.valueOf(amount.getText()),
                        BigDecimal.valueOf(Double.valueOf(price.getText()))
                ));

                Stage stage = (Stage) button.getScene().getWindow();
                stage.close();

                adminAppController.refreshProductsTable();
            });
        } else {
            button.setText("Изменить");

            button.setOnAction(event -> {
                productDao.update(selectedProduct.getId(), new Product(
                        name.getText(),
                        Integer.valueOf(amount.getText()),
                        BigDecimal.valueOf(Double.valueOf(price.getText()))
                ));

                Stage stage = (Stage) button.getScene().getWindow();
                stage.close();

                adminAppController.refreshProductsTable();
            });
        }
    }
}
