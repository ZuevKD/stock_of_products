package ru.zuevkirill.stock.app;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import ru.zuevkirill.stock.dao.AcceptanceDao;
import ru.zuevkirill.stock.dao.CommonRequests;
import ru.zuevkirill.stock.dao.DishDao;
import ru.zuevkirill.stock.dao.IngredientDao;
import ru.zuevkirill.stock.dao.ProductDao;
import ru.zuevkirill.stock.dao.ProviderDao;
import ru.zuevkirill.stock.dao.SellingDao;
import ru.zuevkirill.stock.entities.Acceptance;
import ru.zuevkirill.stock.entities.Dish;
import ru.zuevkirill.stock.entities.Product;
import ru.zuevkirill.stock.entities.Provider;
import ru.zuevkirill.stock.entities.Selling;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

public class AdminAppController {
    @FXML
    private Button editProvider;
    @FXML
    private Button removeProvider;
    @FXML
    private TableView<Provider> tableProviders;
    @FXML
    private Button addProvider;
    @FXML
    private TableColumn<Provider, String> columnProviders;
    @FXML
    private Button removeProduct;
    @FXML
    private Button addProduct;
    @FXML
    private Button editProduct;
    @FXML
    private TableColumn<Product, String> columnProduct;
    @FXML
    private Tab productsTab;
    @FXML
    private TableColumn<Product, Integer> columnProductAmount;
    @FXML
    private TableColumn<Product, BigDecimal> columnProductPrice;
    @FXML
    private TableView<Product> tableProducts;
    @FXML
    private TableColumn<Product, String> columnAcceptProduct;
    @FXML
    private TableColumn<Acceptance, Date> columnAcceptDate;
    @FXML
    private ComboBox<Product> acceptProduct;
    @FXML
    private TextField acceptAmount;
    @FXML
    private DatePicker acceptDate;
    @FXML
    private Button acceptButton;
    @FXML
    private TableColumn<Acceptance, Integer> columnAcceptAmount;
    @FXML
    private ComboBox<Provider> acceptProvider;
    @FXML
    private TableView<Acceptance> tableAccept;
    @FXML
    private TableColumn<Provider, String> columnAcceptProvider;
    @FXML
    private Label acceptWarning;
    @FXML
    private Tab providersTab;
    @FXML
    private TableColumn<Dish, String> columnCostDish;
    @FXML
    private Button removeDish;
    @FXML
    private Button addDish;
    @FXML
    private TableColumn<Dish, String> columnDish;
    @FXML
    private Button editDish;
    @FXML
    private TableView<Dish> tableDish;
    @FXML
    private TableColumn<Selling, String> columnSellingDate;
    @FXML
    private TextField dishAmount;
    @FXML
    private Button sellingButton;
    @FXML
    private Label warning;
    @FXML
    private TableColumn<?, ?> columnSellingAmount;
    @FXML
    private DatePicker sellingDate;
    @FXML
    private ComboBox<Dish> dishChoice;
    @FXML
    private Label amountPrice;
    @FXML
    private TableView<Selling> tableSellingDish;
    @FXML
    private TableColumn<Selling, String> columnSellingCosts;
    @FXML
    private TableColumn<Selling, Selling> columnSellingDish;
    @FXML
    private Button buttonCalculate;

    private ObservableList<Provider> providers = FXCollections.observableArrayList();
    private ObservableList<Product> products = FXCollections.observableArrayList();
    private ObservableList<Acceptance> acceptances = FXCollections.observableArrayList();
    private ObservableList<Dish> dishes = FXCollections.observableArrayList();
    private ObservableList<Dish> selectDishes = FXCollections.observableArrayList();
    private ObservableList<Selling> sellingDishes = FXCollections.observableArrayList();
    private ObservableList<Selling> sellings = FXCollections.observableArrayList();

    private ProviderDao providerDao;
    private ProductDao productDao;
    private AcceptanceDao acceptanceDao;
    private DishDao dishDao;
    private IngredientDao ingredientDao;
    private SellingDao sellingDao;

    @FXML
    public void initialize() {
        columnProviders.setCellValueFactory(new PropertyValueFactory<>("name"));
        columnProduct.setCellValueFactory(new PropertyValueFactory<>("name"));
        columnProductAmount.setCellValueFactory(new PropertyValueFactory<>("amount"));
        columnProductPrice.setCellValueFactory(new PropertyValueFactory<>("price"));

        SimpleDriverDataSource ds = Utils.getDataSource();

        providerDao = new ProviderDao(ds);
        productDao = new ProductDao(ds);
        acceptanceDao = new AcceptanceDao(ds);
        dishDao = new DishDao(ds);
        ingredientDao = new IngredientDao(ds);
        sellingDao = new SellingDao(ds);

        providers.addAll(providerDao.getRecords());
        tableProviders.setItems(providers);
        acceptProvider.setItems(providers);

        products.addAll(productDao.getRecords());
        tableProducts.setItems(products);
        acceptProduct.setItems(products);

        refreshAcceptTable();
        refreshDishTable();
        refreshSellingTable();

        Pattern p = Pattern.compile("(\\d+)?");

        acceptAmount.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!p.matcher(newValue).matches()) acceptAmount.setText(oldValue);
        });

        dishAmount.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!p.matcher(newValue).matches()) acceptAmount.setText(oldValue);
        });

        buttonCalculate.setOnAction(event -> {
            if (isNotFillComboBox(dishChoice)
                    && isNotFillField(dishAmount)) {
                return;
            }

            BigDecimal price = CommonRequests.getSumDish(dishChoice.getValue().getId());
            amountPrice.setText("Общая стоимость: " + price);
        });

        sellingButton.setOnAction(event -> {
            if (isNotFillComboBox(dishChoice)
                    && isNotFillField(dishAmount)
                    && isNotFillDate(sellingDate)) {
                showWarningSelect("Заполните все поля");
            }

            sellingDao.add(new Selling(
                    Integer.valueOf(dishAmount.getText()),
                    dishChoice.getValue(),
                    Date.valueOf(sellingDate.getValue()),
                    CommonRequests.getSumDish(dishChoice.getValue().getId())
            ));

            refreshSellingTable();
        });

        addProvider.setOnAction(event -> {
            TextInputDialog dialog = new TextInputDialog();
            dialog.setTitle("Добавление поставщика");
            dialog.setHeaderText(null);
            dialog.setContentText("Введите поставщика:");

            Optional<String> result = dialog.showAndWait();
            result.ifPresent(name -> providerDao.add(new Provider(name)));

            providers.clear();
            providers.addAll(providerDao.getRecords());

            tableProviders.setItems(providers);
            acceptProvider.setItems(providers);
        });

        addProduct.setOnAction(event -> {
            Stage stage = new Stage();

            try {
                addProductController(stage, TypeEditor.ADD, -1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        addDish.setOnAction(event -> {
            Stage stage = new Stage();

            try {
                addDishController(stage, TypeEditor.ADD, -1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        editProvider.setOnAction(event -> {
            Provider selectedProvider = tableProviders.getSelectionModel().getSelectedItem();

            if (selectedProvider == null) {
                showWarningSelect("Выберите поставщика");
                return;
            }

            TextInputDialog dialog = new TextInputDialog(selectedProvider.getName());
            dialog.setTitle("Изменение поставщика");
            dialog.setHeaderText(null);
            dialog.setContentText("Измените имя поставщика:");

            Optional<String> result = dialog.showAndWait();
            result.ifPresent(name -> providerDao.update(selectedProvider.getId(), new Provider(name)));

            providers.clear();
            providers.addAll(providerDao.getRecords());

            tableProviders.setItems(providers);
        });

        editProduct.setOnAction(event -> {
            Stage stage = new Stage();

            Product product = tableProducts.getSelectionModel().getSelectedItem();
            try {
                if (product != null) {
                    addProductController(stage, TypeEditor.EDIT, product.getId());
                } else {
                    showWarningSelect("Выберите продукт");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        editDish.setOnAction(event -> {
            Stage stage = new Stage();

            Dish dish = tableDish.getSelectionModel().getSelectedItem();
            try {
                if (dish != null) {
                    addDishController(stage, TypeEditor.EDIT, dish.getId());
                } else {
                    showWarningSelect("Выберите блюдо");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        removeProvider.setOnAction(event -> {
            Provider selectedProvider = tableProviders.getSelectionModel().getSelectedItem();

            if (selectedProvider == null) {
                showWarningSelect("Выберите поставщика");
                return;
            }

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Удаление поставщика");
            alert.setHeaderText(null);
            alert.setContentText("Удалить поставщика " + selectedProvider.getName());

            Optional<ButtonType> result = alert.showAndWait();

            if (result.get() == ButtonType.OK) {
                providerDao.remove(selectedProvider.getId());
            }

            providers.clear();
            providers.addAll(providerDao.getRecords());

            tableProviders.setItems(providers);
        });

        removeProduct.setOnAction(event -> {
            Product selectedProduct = tableProducts.getSelectionModel().getSelectedItem();

            if (selectedProduct == null) {
                showWarningSelect("Выберите продукт");
                return;
            }

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Удаление продукта");
            alert.setHeaderText(null);
            alert.setContentText("Удалить продукт " + selectedProduct.getName());

            Optional<ButtonType> result = alert.showAndWait();

            if (result.get() == ButtonType.OK) {
                productDao.remove(selectedProduct.getId());
            }

            products.clear();
            products.addAll(productDao.getRecords());

            tableProducts.setItems(products);
            acceptProduct.setItems(products);
        });

        removeDish.setOnAction(event -> {
            Dish dish = tableDish.getSelectionModel().getSelectedItem();

            if (dish == null) {
                showWarningSelect("Выберите блюдо");
                return;
            }

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Удаление блюда");
            alert.setHeaderText(null);
            alert.setContentText("Удалить блюдо " + dish.getName());

            Optional<ButtonType> result = alert.showAndWait();

            if (result.get() == ButtonType.OK) {
                ingredientDao.remove(dish.getId());
                dishDao.remove(dish.getId());
            }

            refreshDishTable();
        });

        acceptButton.setOnAction(event -> {
            acceptWarning.setVisible(false);

            if (isNotFillComboBox(acceptProduct)
                    || isNotFillComboBox(acceptProvider)
                    || isNotFillDate(acceptDate)
                    || isNotFillField(acceptAmount)) {
                acceptWarning.setVisible(true);
                return;
            }

            Product selectedProduct = acceptProduct.getValue();
            Provider selectedProvider = acceptProvider.getValue();
            Date date = Date.valueOf(acceptDate.getValue());
            Integer amount = Integer.valueOf(acceptAmount.getText());

            Acceptance acceptance = new Acceptance(date, selectedProvider, selectedProduct, amount);
            acceptanceDao.add(acceptance);

            productDao.update(selectedProduct.getId(), new Product(
                    selectedProduct.getName(),
                    selectedProduct.getAmount() + amount,
                    selectedProduct.getPrice()
            ));

            refreshAcceptTable();
            refreshProductsTable();
        });
    }

    protected void addProductController(Stage stage, TypeEditor typeEditor, long idProduct) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getClassLoader().getResource("editProduct.fxml"));
        loader.load();

        EditProductController editProductController = loader.getController();
        editProductController.setAdminController(this);

        if (typeEditor == TypeEditor.EDIT) {
            editProductController.setProductId(idProduct);
        }
        editProductController.setTypeEditor(typeEditor);

        Parent root = loader.getRoot();

        if (typeEditor == TypeEditor.ADD) {
            stage.setTitle("Добавление продукта");
        } else if (typeEditor == TypeEditor.EDIT) {
            stage.setTitle("Изменение продукта");
        }

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    protected void addDishController(Stage stage, TypeEditor typeEditor, long idDish) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getClassLoader().getResource("editDish.fxml"));
        loader.load();

        EditDishController editDishController = loader.getController();
        editDishController.setAdminController(this);

        if (typeEditor == TypeEditor.EDIT) {
            editDishController.setDishId(idDish);
        }
        editDishController.setTypeEditor(typeEditor);

        Parent root = loader.getRoot();

        if (typeEditor == TypeEditor.ADD) {
            stage.setTitle("Добавление блюда");
        } else if (typeEditor == TypeEditor.EDIT) {
            stage.setTitle("Изменение блюда");
        }

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void refreshProductsTable() {
        columnProduct.setCellValueFactory(new PropertyValueFactory<>("name"));
        columnProductAmount.setCellValueFactory(new PropertyValueFactory<>("amount"));
        columnProductPrice.setCellValueFactory(new PropertyValueFactory<>("price"));

        List<Product> productsInDB = productDao.getRecords();

        products.clear();
        products.addAll(productsInDB);

        tableProducts.setItems(products);
    }

    public void refreshAcceptTable() {
        columnAcceptProduct.setCellValueFactory(new PropertyValueFactory<>("product"));
        columnAcceptAmount.setCellValueFactory(new PropertyValueFactory<>("amount"));
        columnAcceptProvider.setCellValueFactory(new PropertyValueFactory<>("provider"));
        columnAcceptDate.setCellValueFactory(new PropertyValueFactory<>("acceptDate"));

        List<Acceptance> acceptanceFromTable = acceptanceDao.getRecords();

        acceptances.clear();
        acceptances.addAll(acceptanceFromTable);

        tableAccept.setItems(acceptances);
    }

    public void refreshDishTable() {
        columnDish.setCellValueFactory(new PropertyValueFactory<>("name"));
        columnCostDish.setCellValueFactory(new PropertyValueFactory<>("costs"));

        List<Dish> dishFromTable = dishDao.getRecords();

        dishes.clear();
        dishes.addAll(dishFromTable);

        tableDish.setItems(dishes);
        dishChoice.setItems(dishes);
    }

    public void refreshSellingTable() {
        columnSellingAmount.setCellValueFactory(new PropertyValueFactory<>("amount"));
        columnSellingDish.setCellValueFactory(new PropertyValueFactory<>("dish"));
        columnSellingDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        columnSellingCosts.setCellValueFactory(new PropertyValueFactory<>("amountCosts"));

        List<Selling> sellingDaoRecords = sellingDao.getRecords();

        sellings.clear();
        sellings.addAll(sellingDaoRecords);

        tableSellingDish.setItems(sellings);
    }


    private void showWarningSelect(String warning) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Ошибка");
        alert.setHeaderText(null);
        alert.setContentText(warning);

        alert.showAndWait();
    }

    private boolean isNotFillComboBox(ComboBox comboBox) {
        return comboBox.getValue() == null;
    }

    private boolean isNotFillField(TextField field) {
        return field.getText() == null
                || field.getText().trim().isEmpty();
    }

    private boolean isNotFillDate(DatePicker dateField) {
        return dateField.getValue() == null
                || dateField.getValue().toString().trim().isEmpty();
    }
}
