package ru.zuevkirill.stock.entities;

import java.sql.Date;

public class Acceptance {
    private long id;
    private Date acceptDate;
    private Provider provider;
    private Product product;
    private int amount;

    public Acceptance(Date acceptDate, Provider provider, Product product, int amount) {
        this.acceptDate = acceptDate;
        this.provider = provider;
        this.product = product;
        this.amount = amount;
    }

    public Acceptance(long id, Date acceptDate, Provider provider, Product product, int amount) {
        this.id = id;
        this.acceptDate = acceptDate;
        this.provider = provider;
        this.product = product;
        this.amount = amount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getAcceptDate() {
        return acceptDate;
    }

    public void setAcceptDate(Date acceptDate) {
        this.acceptDate = acceptDate;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
