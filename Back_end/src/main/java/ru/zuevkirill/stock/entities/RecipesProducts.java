package ru.zuevkirill.stock.entities;

public class RecipesProducts {
    private long id;
    private long idDish;
    private long idRecipe;
    private long idProduct;
    private int amount;

    public RecipesProducts(long idDish, long idRecipe, long idProduct, int amount) {
        this.idDish = idDish;
        this.idRecipe = idRecipe;
        this.idProduct = idProduct;
        this.amount = amount;
    }

    public RecipesProducts(long id, long idDish, long idRecipie, long idProduct, int amount) {
        this.id = id;
        this.idDish = idDish;
        this.idRecipe = idRecipie;
        this.idProduct = idProduct;
        this.amount = amount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdDish() {
        return idDish;
    }

    public void setIdDish(long idDish) {
        this.idDish = idDish;
    }

    public long getIdRecipie() {
        return idRecipe;
    }

    public void setIdRecipie(long idRecipe) {
        this.idRecipe = idRecipe;
    }

    public long getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(long idProduct) {
        this.idProduct = idProduct;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
