package ru.zuevkirill.stock.entities;

import java.math.BigDecimal;

public class Dish {
    private long id;
    private String name;
    private BigDecimal costs;

    public Dish(String name, BigDecimal costs) {
        this.name = name;
        this.costs = costs;
    }

    public Dish(long id, String name, BigDecimal costs) {
        this.id = id;
        this.name = name;
        this.costs = costs;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getCosts() {
        return costs;
    }

    public void setCosts(BigDecimal costs) {
        this.costs = costs;
    }

    @Override
    public String toString() {
        return name;
    }
}
