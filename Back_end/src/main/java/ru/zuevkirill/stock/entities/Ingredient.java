package ru.zuevkirill.stock.entities;

public class Ingredient {
    private long id;
    private Dish dish;
    private Product product;
    private int amount;

    public Ingredient(Dish dish, Product product, int amount) {
        this.dish = dish;
        this.product = product;
        this.amount = amount;
    }

    public Ingredient(long id, Dish dish, Product product, int amount) {
        this.id = id;
        this.dish = dish;
        this.product = product;
        this.amount = amount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}


