package ru.zuevkirill.stock.entities;

import java.math.BigDecimal;
import java.sql.Date;

public class Selling {
    private long id;
    private int amount;
    private Dish dish;
    private Date date;
    private BigDecimal amountCosts;

    public Selling(int amount, Dish dish, Date date, BigDecimal amountCosts) {
        this.amount = amount;
        this.dish = dish;
        this.date = date;
        this.amountCosts = amountCosts;
    }

    public Selling(long id, int amount, Dish dish, Date date, BigDecimal amountCosts) {
        this.id = id;
        this.amount = amount;
        this.dish = dish;
        this.date = date;
        this.amountCosts = amountCosts;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getAmountCosts() {
        return amountCosts;
    }

    public void setAmountCosts(BigDecimal amountCosts) {
        this.amountCosts = amountCosts;
    }
}
