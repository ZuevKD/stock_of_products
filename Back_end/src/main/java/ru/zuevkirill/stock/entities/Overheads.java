package ru.zuevkirill.stock.entities;

public class Overheads {
    private long id;
    private int precent;

    public Overheads(int precent) {
        this.precent = precent;
    }

    public Overheads(long id, int precent) {
        this.id = id;
        this.precent = precent;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPrecent() {
        return precent;
    }

    public void setPrecent(int precent) {
        this.precent = precent;
    }
}
