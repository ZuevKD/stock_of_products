package ru.zuevkirill.stock.entities;

public class Provider {
    private long id;
    private String name;

    public Provider() {
    }

    public Provider(String name) {
        this.name = name;
    }

    public Provider(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
